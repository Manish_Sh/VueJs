<?php include "include/header.php" ?>

	<div id="app">
		<div>
			<h2>v-if and v-else syntax</h2>
			<p v-if="show">You can see me! (v-if stay is true)</p>
			<p v-else>Now you see me! (if is false and v-else get execute)</p>
			<p>Do you also see me</p><br>
			<button @click="show = !show">Switch</button>
		</div>	<hr>
		<div>
			<p>loop the data with v-for syntax with value,key</p>
			<ul>
				<li v-for="(ingredient , i) in ingredients">{{ ingredient }} ({{i}})</li>
			</ul>
			<button @click="ingredients.push('spices')">Push IN</button>
			<button @click="ingredients.pop('spices')">Pop OUT</button>

			<p>loop the data in object: value form (Loop through objects outside of nested loops, too!)</p>
			<ul>
				<li v-for="person in persons">
					<div v-for="(value,key,i) in person">{{ key }} : {{ value }} ({{ i }})</div>
				</li>
			</ul>

			<span v-for="n in 10">{{ n }}_</span>
		</div>
	</div>

<?php include "include/footer.php" ?>