<?php include "include/header.php" ?>

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div id="app">
        	<p v-once>{{ title }} - <a v-bind:href="link" target="_blank">Google</a></p><br>
            <b>Note: v-bind:href bind data can also be bind as :href</b><hr>

        	<p>{{ text }}</p><hr>

        	<p>{{ sayHello() }}</p><hr>

        	v-on:input="EnterName" -> data Binding '{/{name}/}' from method EnterName : <input type="text" v-on:input="EnterName" placeholder="Name">{{ name }}<br>
        	v-model="name" -> two way data Binding done by v-model :
        	<input type="text" v-model="name">{{ name }}<hr>

        	<p>{{ title }}</p><hr>

        	<p v-html="finishedLink"></p><hr>

        	<h2>EVENTS</h2>
        	<button v-on:click="increase" :class="{red: attachedRed}">Click</button>
        	<p>
        		v-on:click -> {{counter_1}}<br>
				Expression '*2' written in interpolation -> {{counter_1 * 2}}
        	</p>
        	<p>
        		<button v-on:click="counter_2++,counter_3++">Increase</button>
        		<button v-on:click="counter_2--,counter_3--">Decrease</button>
        		
        		<mark>Counter : {{counter_2}}</mark>
        		<mark>Result : {{result() }}</mark>
        		<span> It will reset the counter after a few sec. It will work as a async. by watch object <mark>{{ counter_3 }}</mark></span><br>

                <b>Note: v-on:click event can also be listen by @click</b>
                
        	</p>
        	<p v-on:mousemove="updateCorordinate">v-on:mousemove -> OnMouseMove corordinate check : {{x}}/{{y}} <mark v-on:mousemove.stop="">"Stop Propagation by using event modifier (.stop) v-on:mousemove.stop"</mark> <mark v-on:mousemove="DontMove">"Stop Propagation by event.stopPropagation()"</mark></p><hr>

        	<p>
        		<h2>Key Event modifier</h2>
				<input type="text" v-on:keyup="alert"/> Give alert on every keyup <br>
				<input type="text" v-on:keyup.enter.space.esc="alert"/> Give alert on Enter, space and Esc on Keyup
        	</p><hr>
            
            <h2>Dynamic Styling witg CSS class</h2>
            <h2>Add a class to div on @click</h2>
            <div class="color_block">
                <div class="demo" @click="attachedRed = !attachedRed" :class="{red: attachedRed}"></div>
                <div class="demo" @click="attachedGreen = !attachedGreen" :class="{green: attachedGreen,red: attachedRed}"></div>
                <div class="demo" @click="attachedYellow = !attachedYellow" :class="{yellow: attachedYellow}"></div>
            </div>
            
            <h2>Add a class ".divClasses" to a div with the computed method</h2>
            <div class="color_block">
                <div class="demo" @click="attachedRed = !attachedRed" :class="{red: attachedRed}"></div>
                <div class="demo" :class="divClasses"></div>
                <div class="demo" @click="attachedYellow = !attachedYellow" :class="{yellow: attachedYellow}"></div>
            </div>

            <h2>Change class on Dynamic</h2>
            <div class="color_block">
                <div class="demo" :class="color"></div><br>
                on input type class will get updated dynamic which is defined in data property and css class: <input type="text" v-model="color"><br>
            </div>

            <h2>Add a style "myStyle" to a div with the computed method</h2>
            <div class="color_block">
                <div class="demo" :style="{backgroundColor: color_change}"></div>
                <div class="demo" :style="myStyle"></div>
                <div class="demo" :style="[myStyle , {height: width + 'px'}]"></div><br>
                Type any color : <input type="text" v-model="color_change"><br>
                can Change a width in pixel : <input type="number" v-model="width"><br>
            </div>
            <ul>
                <li> 1st block is set the style as by input color and first color is set with data property</li>
                <li> 2nd block is set the style as by input color and change width as defined in computed method</li>
                <li> 3rd block is set the a height as same as width and style is set in a form of array [myStyle, {height: width + 'px'}]</li>
            </ul>
        </div>

		
<?php include "include/footer.php" ?>