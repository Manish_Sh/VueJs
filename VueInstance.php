<?php include "include/header.php" ?>
 
 	<h2>Vue instance lifeCycle</h2>
 	<div id='VueLifecylce'>
 		<h1> {{ title }} </h1>
 		<button @click="title = 'change'">Update</button>
 		<button @click="destroy">Destroy</button>
 	</div>

<?php include "include/footer.php" ?>