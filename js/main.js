$(document).ready(function(){
	if($('#app1').length > 0){
		new Vue({
			el: '#app',
			data: {
				title : "hello",
				text: "Hello World",
				link: "http://www.google.com",
				name: "Manish",
				finishedLink: "<a href='http://google.com'>Google</a> 'achor tag is rendered by v-html'",
				counter_1: 0,
				counter_2: 0,
				counter_3: 0,
				x: 0,
				y: 0,
				secoundCOunter: 0,
				attachedRed: false,
				attachedGreen: false,
				attachedYellow: false,
				color: 'green',
				color_change: 'gray',
				width: 100,
				CONDITIONAL_START: 'START',
				show: true,
				ingredients: ['meat','fruit','cookies',],
				persons: [{'name':'Manish','age':'27','color':'red'},
						  {'name':'Sunny','age':'unknown','color':'blue'}],
						  hello: "hello"
			},
			computed: {
				divClasses : function(){
					return {
						red: this.attachedRed,
						yellow: !this.attachedRed
					};
				},
				myStyle: function(){
					return{
						backgroundColor: this.color_change,
						width: this.width + 'px'
					};
				}
			},
			watch: {
				counter_2: function(value){
					var vm = this;
					setTimeout(function(){
						vm.counter_3 = 0;
					},5000);
				}
			},
			methods: {
				sayHello : function(){
					this.title = "v-once get activated and 'hello' is replaced"
					return this.title + " by " + this.text;
				},
				EnterName: function(){
					this.name = event.target.value;
				},
				increase: function(){
					this.counter_1++;
					console.log(this.counter_1);
				},
				updateCorordinate: function(event){
					this.x = event.clientX;
					this.y = event.clientY;
				},
				DontMove: function(event){
					event.stopPropagation();
				},
				alert: function(){
					alert('Key Event');
				},
				result: function() {
					return this.counter_2 > 5 ? "Greater then 5" : "Smaller then 5";
				}
			}
		});
	}
	

	/* ---------------------------------
	//	APP2
	*/

	if (document.querySelector('#app2')){	
		new Vue({
			el: '#app2',
			data:{
				PlayerHealth: '100',
				MonsterHealth: '100',
				gameIsRunning: false,
				turns: []
			},
			methods: {
				startGame : function(){
					this.gameIsRunning = true;
					this.PlayerHealth = '100';
					this.MonsterHealth = '100';
					this.turns = [];
				},
				attack : function(){
					var damage = this.CalculateDamage(3,10);
					this.MonsterHealth -= damage;
					this.turns.unshift({
						isPlayer: true,
						text: 'Players hits Monster by ' + damage
					});
					
					// CheckWin() is we chould check we sholud continue in this function or not
					if(this.CheckWin()){ 
						return;
					}

					this.monsterAttck();

				},
				CalculateDamage : function(min , max){ //CalculateDamage is called up in attack function
					return Math.max(Math.floor(Math.random() * 10) + 1, min);
				},
				CheckWin : function(){ //CheckWin is called up in attack function
					if(this.MonsterHealth <= 0){
						if(confirm('You Won! New Game?')){
							this.startGame(); //start A new Game.
						}else {
							this.gameIsRunning = false;
						}
						return true; // call a method CheckWin() in attack function
					}else if(this.PlayerHealth <= 0){
						if(confirm('You Lost! New Game?')){
							this.startGame(); //start A new Game.
						}else{
							this.gameIsRunning = false;
						}
						return true; // call a method CheckWin() in attack function
					}
					return false;
				},
				monsterAttck : function(){
					var damage = this.CalculateDamage(5,12);
					this.PlayerHealth -= damage;
					this.CheckWin();

					this.turns.unshift({
						isPlayer: false,
						text: 'Monster hits Player for ' + damage
					});
				},
				special_attack : function(){
					var damage = this.CalculateDamage(10,20);
					this.MonsterHealth -= damage;
					this.turns.unshift({
						isPlayer: true,
						text: 'Players hits Monster hard by ' + damage
					});
					if(this.CheckWin()){ 
						return;
					}
					this.monsterAttck();
				},
				heal : function(){
					if(this.PlayerHealth <= 90){
							this.PlayerHealth += 10;
					}else{
							this.PlayerHealth = 100;
					}

					this.turns.unshift({
						isPlayer: true,
						text: 'Player heals for 10'
					});

					this.monsterAttck();
				},
				give_up : function(){
					this.gameIsRunning = false;
				}
			}
		});
	}


	/* ---------------------------------
	//	Vue Instances
	*/

	if (document.querySelector("#VueLifecylce")){	
		new Vue ({
			el: '#VueLifecylce',
			data: {
				title: 'Hello World!'
			},
			beforeCreate: function(){
				console.log('beforeCreate()');
			},	
			created: function(){
				console.log('created()');
			},	
			beforeMount: function(){
				console.log('beforeMount()');
			},	
			mounted: function(){
				console.log('mounted()');
			},	
			beforeUpdate: function(){
				console.log('beforeUpdate()');
			},	
			updated: function(){
				console.log('updated()');
			},	
			beforeDestroy: function(){
				console.log('beforeDestroy()');
			},	
			destroyed: function(){
				console.log('destroyed()');
			},
			methods: {
				destroy: function(){
					this.$destroy()
				}
			}
		});
	}

});

